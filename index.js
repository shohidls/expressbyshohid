var express = require('express');
var app = express();

var things = require('./things.js');

app.get('/', function(req, res){
   res.send("Hello world!");
});

app.get('/hello', function(req, res){
   res.send("Helloyyy World!");
});

app.get('/:id', function(req, res){
   res.send('The id you specified is ' + req.params.id);
});


app.get('/things/:name/:id', function(req, res) {
   res.send('id: ' + req.params.id + ' and name: ' + req.params.name);
});
//http://localhost:3000/things/tutorialspoint/12345.


app.get('/things/:id([0-9]{5})', function(req, res){
   res.send('id: ' + req.params.id);
});
//http://localhost:3000/things/56548
//only if i input any 5 digit number


app.use('/things', things);

app.listen(3000);